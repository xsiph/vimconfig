# What is this?

My personal vim config, assembled with `Nix`, and using vim2nix.

This is forked from https://www.github.com/DieracDelta/vimconfig

# Usage

```
nix run gitlab:xsiph/vimconfig
```
