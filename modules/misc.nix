{ pkgs, dsl, ... }:
with dsl; {
  plugins = with pkgs; [
    # sick parinfer mode fast asf
    parinfer-rust-nvim
    # paren stuffs
    vimPlugins.surround-nvim
  ];

  use.surround.setup = callWith {
    context_offset = 100;
    load_autogroups = false;
    mappings_style = "sandwich";
    map_insert_mode = true;
    quotes = ["'" "\""];
    brackets = ["(" "{" "["];
    space_on_closing_char = false;
    pairs = {
      nestable = {
        b = [ "(" ")" ];
        s = [ "[" "]" ];
        B = [ "{" "}" ];
        a = [ "<" ">" ];
      };
      linear = {
        q = [ "'" "'" ];
        t = [ "`" "`" ];
        d = [ "\"" "\"" ];
      };
    };
    prefix = "s";
  };

}
