{ pkgs, dsl, ... }:
with dsl; {
  plugins = with pkgs; [
    vimPlugins.neogit
    vimPlugins.diffview-nvim
    vimPlugins.gitsigns-nvim
  ];
  setup.gitsigns = { };
  setup.neogit = {
    signs = {
      section = [ "" "" ];
      item = [ "" "" ];
    };

    integrations.diffview = true;
  };
}
